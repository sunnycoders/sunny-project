import React from "react";
import FooterEmail from "./footer/Footer_email";
import Footer1 from './footer/Footer1';
// import Footer2 from './footer/Footer2'
import Footer3 from './footer/Footer3';
import Footer4 from './footer/Footer4';
import CopyRight from './footer/CopyRight';

function Footer() {
  return (
    <>
      <footer data-scroll-section>
        <div
          className="footer__background background-image"
          data-scroll
          data-scroll-speed="-6"
          style={{ backgroundImage: `url(/assets/img/general/footer.jpg)` }}
        ></div>
        <div className="overlay2"></div>
        <div className="" data-scroll data-scroll-speed="-4">
          <FooterEmail />
          <div className="address menu_fade">
            <div className="text">
              <div className="textrow">
                <Footer1 />
                {/* <Footer2 /> */}
                <Footer3 />
                <Footer4 />
              </div>
              <CopyRight />
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}

export default Footer;