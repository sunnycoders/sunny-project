import React from "react";

function FooterEmail() {
  return (
    <>
   <div id="inner_footer" >
      <a id="" href="mailto:contact@webthatup.com" className="main_link menu_fade">
       contact@webthatup.com
      </a>
      <a id="" href="mailto:contact@webthatup.com" className="main_link menu_fade mobile">
       Get in touch
      </a>
    </div>
    </>
  );
}

export default FooterEmail;