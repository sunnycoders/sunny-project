import React from "react";

function Footer1() {
  return (
    <>
      <div className="textcol">
        <h5>
          <a href="#" className="" title="WebthtatUp — India Digital Agency">
            India
          </a>
        </h5>
        <p>
          <strong>Webthatup</strong>
          <br />
          109 4th Floor Ashoka Enclave<br></br> Part - 3 Faridabad, Haryana - 121003
        </p>
      </div>
    </>
  );
}

export default Footer1;