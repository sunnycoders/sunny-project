import React from "react";
import { NavLink } from "react-router-dom";
function Footer4() {
  return (
    <>
        <div className="textcol mob_half">
          <h5>
            <a href="brands" title="Brands" className="inner_link">
              Brands <sup>110</sup>
            </a>
            <a href="careers" title="Careers" className="inner_link">
              Careers <sup>04</sup>
            </a>
            <a href="inquiries" title="Inquire" className="inner_link">
              Inquiries
            </a>
          </h5>
        </div>
    </>
  );
}

export default Footer4;