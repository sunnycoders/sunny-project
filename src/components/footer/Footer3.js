import React from "react";
import { a } from "react-router-dom";
function Footer3() {
  return (
    <>
    <div className="textcol mob_half">
        <h5>
            <a href="/" title="Home" className="inner_link">Home</a>
            <a href="/works" title="Works" className="inner_link">Works <sup>12</sup></a>
            <a href="/expertise" title="Expertise" className="inner_link">Expertise <sup>06</sup></a>
            <a href="/about" title="About" className="inner_link">About</a>
        </h5>
        </div>
    </>
  );
}

export default Footer3;