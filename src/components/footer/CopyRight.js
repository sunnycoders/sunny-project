import React from "react";

function CopyRight() {
  return (
    <>
     <p>       
      <span id="copyrightYears">Copyright © 2022. All Rights Reserved By webthatup.com</span> 
    </p>
    </>
  );
}

export default CopyRight;