import React from "react";
import { NavLink, Navigate, useLocation } from "react-router-dom";
import MainNav from "./MainNav";
import SideBarNav from "./SideBarNav";
function Header() {
  const navTitle = [
    {
      id: "1",
      title: "Home",
      link: "/",
    },
    {
      id: "2",
      title: "About",
      link: "/about",
    },
    {
      id: "3",
      title: "Work",
      link: "/works",
    },
    {
      id: "4",
      title: "Brands",
      link: "/brands",
    },
    {
      id: "5",
      title: "Inquiries",
      link: "/inquiries",
    },
  ];
  //const navigate = useNavigate();
  const location = useLocation();

  return (
    <>
      <MainNav />
      <header>
        <div className="header-wrap">
          <NavLink
            to="/"
            className="logo"
            id="main-logo"
            title="Webthatup — Digital Agency"
          >
            <div className="logo-wrap" id="logo-wrap">
              <img
                src="/assets/img/general/brand1.png"
                title="WebthatUp — Digital Agency"
                height="20px"
              />
              <img
                src="/assets/img/general/logo-brand.png"
                title="WebthatUp — Digital Agency"
                height="20px"
              />
            </div>
          </NavLink>
          <div className="nav_right">
            {navTitle.map(({ title, id, link }) => (
              <NavLink
                to={link}
                key={id}
                className=" font_caps s"
              >
                {title}
              </NavLink>
            ))}
          </div>
        </div>
        <div className="nav-menu" id="nav">
          <span className="line"></span>
          <span className="line"></span>
          <span className="text">Menu</span>
        </div>
      </header>
      <SideBarNav />
    </>
  );
}

export default Header;
