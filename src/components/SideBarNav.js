import React from "react";

function SideBarNav() {
  const sidebarLinksArr = [
    { id: "1", title: "Home" },
    {
      id: "2",
      title: "Work",
    },
    {
      id: "3",
      title: "Expertise",
    },
    {
      id: "4",
      title: "About",
    },
  ];
  return (
    <>
      <div className="menu-cover" id="menu-cover"></div>
      <div className="menu-overlay" id="menu-overlay"></div>
      <div id="menuNav" className="menu">
        <div className="nav-menu close" id="navClose">
          <span className="line"></span>
        </div>
        <div className="mid_line"></div>
        <div className="menu_content" id="le_nav">
          <ol className="grid g3">
            {sidebarLinksArr.map(({ title, id }) => (
              <li key={id} className="">
                <a href="#" className="inner_link">
                  {title}
                </a>
              </li>
            ))}
          </ol>
        </div>
      </div>

      <div id="load_overlay" className="load_overlay"></div>
      <div id="dynamicLoad" className="dynamic_load">
        <div className="progress">
          <div className="bar" id="dynamicBar"></div>
        </div>
      </div>
    </>
  );
}

export default SideBarNav;