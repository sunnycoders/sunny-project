import React from "react";

function Headings() {
  return (
    <>
    <div className="q-container container-inner menu_fade">
        <div className="columns short_container reveal">
            <div className="column q-2-3">
                <h6 className="font_caps">/ Expertise</h6>
                <h3 className="">Elevating <a href="#">Brands</a> through innovation in Digital Transformation.</h3>

            </div>
            <div className="column q-1-3 flex_end flex_right">
                <a href="#" className="magnet">What we do <span className="magnet_circle">
                        <span className="magnet_arrow">&#8594;</span></span></a>
            </div>
        </div>
    </div>
    </>
  );
}

export default Headings;