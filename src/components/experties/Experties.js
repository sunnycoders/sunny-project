import React from "react";

function Experties() {
  // Carousel data array
  const CarouselArr = [
    {
      url: "url(assets/img/expertise/thumbs/marketing.jpg)",
      number: " 01",
      title: "Digital Marketing",
      Content: `Campaign Development, Digital Marketing &amp; Advertising,
                  Search Engine Optimisation, Social Media Management.`,
      link: "#",
    },
    {
      url: "url(assets/img/expertise/thumbs/branding.jpg)",
      number: " 02",
      title: "Branding",
      Content: `Brand Strategy, Brand Identity &amp; Positioning, Visual
                  Language, Brand Messaging, Brand Implementation &amp;
                  Guidelines.`,
      link: "#",
    },
    {
      url: "url(assets/img/expertise/thumbs/ux.jpg)",
      number: " 03",
      title: "User Experience",
      Content: `Experience Strategy, User Interface Design, Design Systems
                  &amp; data Guides, Product Prototyping &amp; Optimization.`,
      link: "#",
    },
    {
      url: "url(assets/img/expertise/thumbs/development.jpg)",
      number: " 04",
      title: "Product Development",
      Content: `Technical Discovery &amp; Architecture, Website Development,
                  eCommerce, Mobile &amp; Web App Development, CRM / ERM
                  Platforms.`,
      link: "#",
    },
    {
      url: "url(assets/img/expertise/thumbs/marketing.jpg)",
      number: " 05",
      title: "Digital Marketing",
      Content: `Campaign Development, Digital Marketing &amp; Advertising,
                  Search Engine Optimisation, Social Media Management.`,
      link: "#",
    },
    {
      url: "url(assets/img/expertise/thumbs/production.jpg)",
      number: " 06",
      title: "Media Production",
      Content: `Art Direction, Photography, Video Production, 360 Photography
                  &amp; Video, Copywriting.`,
      link: "#",
    },
    {
      url: "url(assets/img/expertise/thumbs/strategy.jpg)",
      number: " 07",
      title: "Digital Strategy",
      Content: `Consumer Insights &amp; Trends, Go-To-Market Planning, Data
                  Science &amp; Analytics, Product Strategy &amp; Roadmapping.`,
      link: "#",
    },
  ];
  
  return (
    <>
      <div className="q-container container-inner overflow menu_fade">
        <div className="columns">
          <div className="column" style={{ position: `relative` }}>
            <div className="carousel">
              <div className="carousel__planes">
                {CarouselArr.map(({ url, number, title, Content, link }) => (
                  <div className="carousel__plane" key={number}>
                    <div className="carousel__image" data-hover="Drag">
                      <div className="background-image" data-bg={url}>
                        {" "}
                      </div>
                    </div>
                    <a href={link} className="carousel__text" data-hover="None">
                      <span className="carousel__num font_caps"> / {number}</span>
                      <h4>{title}</h4>
                      <p>{Content}</p>
                    </a>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Experties;
