import React from "react";
const  MainNav = () => {
  return (
    <>
      <div className="cursor" data-cursor>
        <div className="inner">
          <div className="circle">
            <span className="cursor_text"></span>
          </div>
        </div>
      </div>
    </>
  );
}

export default MainNav;