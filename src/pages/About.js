import React from "react";
const About = () => {
    return (
      <>
        <div className="header_animate header_animate_pre" data-scroll-section>
          <section className="section small-pb section__header ">
            <div className="square_banner" style={{ backgroundColor: `#000` }}>
              <div className="image_reveal">
                <div
                  className="background-image"
                  data-scroll
                  data-scroll-speed="-2"
                  data-bg="url('assets/img/about/banner.jpg')"
                ></div>
                <div className="cover"></div>
              </div>
            </div>

            <div className="pre_animate">
              <div className="q-container container-inner">
                <div className="columns">
                  <div className="column menu_fade">
                    <h1 className="xlarge">
                      <span className="reveal_wrap">
                        <span className="pre_reveal">Hello.</span>
                      </span>
                    </h1>
                  </div>
                </div>
              </div>
            </div>
            <div className="q-container container-inner">
              <div className="columns">
                <div className="column menu_fade">
                  <h6 className="font_caps">
                    <span className="reveal_wrap">
                      <span className="reveal_item">/ About</span>
                    </span>
                  </h6>
                  <h2 className="long">
                    <span className="reveal_wrap">
                      <span className="reveal_item">
                        We are Fleava. We create{" "}
                      </span>
                    </span>
                    <span className="reveal_wrap">
                      <span className="reveal_item">
                        <a href="awards.html">award</a>—winning{" "}
                        <a href="works.html">Products</a>,
                      </span>
                    </span>
                    <span className="reveal_wrap">
                      <span className="reveal_item">memorable Campaigns,</span>
                    </span>
                    <span className="reveal_wrap">
                      <span className="reveal_item">
                        &amp; remarkable <a href="brands.html">Brands</a>.
                      </span>
                    </span>
                  </h2>
                  <h6 className="font_caps">
                    <span className="reveal_wrap">
                      <span className="reveal_item">Scroll Down</span>
                    </span>
                  </h6>
                </div>
              </div>
            </div>
          </section>
        </div>

        <section className="section small-pb large-pb" data-scroll-section>
          <div className="q-container container-inner ">
            <div className="columns short_container menu_fade ">
              <div className="column q-1-3 "></div>
              <div className="column q-2-3 ">
                <p className="lead">
                  Based out of{" "}
                  <a href="sg.html" title="Fleava Singapore">
                    Singapore
                  </a>{" "}
                  and{" "}
                  <a href="id.html" title="Fleava Indonesia">
                    Bali, Indonesia
                  </a>
                  , Fleava has become one of the best quality Digital Agency in
                  Asia. Our focus has always been to create enjoyable,
                  intuitive, engaging and remarkable experience for people —
                  that's what sets us apart from everyone else.
                  <br />
                  <br />
                </p>
                <a href="awards.html">
                  <img
                    src="assets/img/awards/badge_white.png"
                    className="nomination"
                    alt="Agency of the Year: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
                  />
                </a>
              </div>
            </div>
          </div>
        </section>

        <section className="section no-p grey" data-scroll-section>
          <div
            className="offset_banner reveal"
            style={{ backgroundColor: `#000` }}
            data-scroll
          >
            <div className="image_reveal">
              <div
                className="background-image"
                data-scroll
                data-scroll-speed="-3"
                style={{ backgroundImage: `url('assets/img/about/about.jpg` }}
              ></div>
            </div>
          </div>
        </section>

        <section className="section no-pt grey" data-scroll-section>
          <div className="q-container container-inner">
            <div className="columns menu_fade short_container reveal">
              <div className="column q-1-3"></div>
              <div className="column q-2-3">
                <div>
                  <p className="lead">
                    We’ve worked incredibly hard to build a talented, industry
                    leading team of professionals. With a team of creative,
                    strategist, business and development specialists, we
                    consistently strive to be at the forefront of new media
                    technology.
                    <br />
                    <br />
                  </p>
                  <a href="expertise.html" className="magnet">
                    What we do{" "}
                    <span className="magnet_circle">
                      <span className="magnet_arrow">&#8594;</span>
                    </span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section
          className="section trails long"
          id="trails"
          data-scroll-section
        >
          <div className="q-container container-inner menu_fade">
            <div className="columns short_container">
              <div className="column" data-scroll data-scroll-speed="-4">
                <h6 className="font_caps">/ Brands</h6>
                <h2 className="">
                  We help businesses
                  <br />
                  all around the world <br />
                  to grow.
                </h2>
                <div className="client_logos">
                  <div className="client-wrap">
                    <img
                      src="/assets/img/about/client_logos/1.webp"
                      alt="Canon Asia"
                      title="Canon Asia"
                    />
                    <img
                      src="/assets/img/about/client_logos/2.webp"
                      alt="McDonalds Manila"
                      title="McDonalds Manila"
                    />
                    <img
                      src="/assets/img/about/client_logos/3.webp"
                      alt="The Bodyshop Indonesia"
                      title="The Bodyshop Indonesia"
                    />
                    <img
                      src="/assets/img/about/client_logos/6.webp"
                      alt="Gojek"
                      title="Gojek"
                    />
                    <img
                      src="/assets/img/about/client_logos/16.webp"
                      alt="MOKA POS"
                      title="MOKA POS"
                    />
                    <img
                      src="/assets/img/about/client_logos/18.webp"
                      alt="Midtrans"
                      title="Midtrans"
                    />
                    <img
                      src="/assets/img/about/client_logos/4.webp"
                      alt="J.Co"
                      title="J.Co"
                    />
                    <img
                      src="/assets/img/about/client_logos/5.webp"
                      alt="Boga Group"
                      title="Boga Group"
                    />
                    <img
                      src="/assets/img/about/client_logos/12.webp"
                      alt="IHG"
                      title="IHG"
                    />
                    <img
                      src="/assets/img/about/client_logos/13.webp"
                      alt="Mariott"
                      title="Mariott"
                    />
                    <img
                      src="/assets/img/about/client_logos/7.webp"
                      alt="SPOTS"
                      title="SPOTS"
                    />
                    <img
                      src="/assets/img/about/client_logos/8.webp"
                      alt="PayTren"
                      title="PayTren"
                    />
                    <img
                      src="/assets/img/about/client_logos/10.webp"
                      alt="Gobiz"
                      title="Gobiz"
                    />
                    <img
                      src="/assets/img/about/client_logos/11.webp"
                      alt="HIEC"
                      title="HIEC"
                    />
                    <img
                      src="/assets/img/about/client_logos/14.webp"
                      alt="Mowilex"
                      title="Mowilex"
                    />
                  </div>
                </div>
                <a href="brands.html" className="magnet bold">
                  Explore all Brands{" "}
                  <span className="magnet_circle">
                    <span className="magnet_arrow">&#8594;</span>
                  </span>
                </a>
              </div>
            </div>
          </div>

          <img
            className="content__img"
            src="assets/img/about/clients/1.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/2.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/3.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/6.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/18.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/4.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/5.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />

          <img
            className="content__img"
            src="assets/img/about/clients/7.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/8.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/9.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />

          <img
            className="content__img"
            src="assets/img/about/clients/16.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/17.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />

          <img
            className="content__img"
            src="assets/img/about/clients/10.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/11.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/12.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/13.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />
          <img
            className="content__img"
            src="assets/img/about/clients/14.webp"
            alt="Client: Fleava - Bali &amp; Jakarta, Singapore Digital Agency"
          />

          <div className="cover_top"></div>
          <div className="cover_bottom"></div>
        </section>

        <section className="section x_hide grey" data-scroll-section>
          <div className="q-container container-inner menu_fade">
            <div className="columns short_container reveal">
              <div className="column q-2-3">
                <h6 className="font_caps">/ Core Values</h6>
                <h3 className="">
                  At the heart of everything we do is this idea of making
                  business better.
                </h3>
              </div>
              <div className="column q-1-3 flex_end flex_right"></div>
            </div>
          </div>

          <div className="q-container container-inner overflow  menu_fade">
            <div className="columns">
              <div className="column">
                <div className="carousel values_slide">
                  <div className="carousel__planes" data-hover="Drag">
                    <div className="carousel__plane">
                      <div className="carousel__image">
                        <div
                          className="background-image"
                          style={{
                            backgroundImage: `url('assets/img/about/value1.jpg')`,
                          }}
                        ></div>
                      </div>
                      <div className="carousel__text">
                        <span className="carousel__num font_caps">/ 01</span>
                        <h4>
                          Never compromise
                          <br />
                          on quality.
                        </h4>
                        <p>
                          We don’t want our brand associated with anything that
                          isn’t of the highest quality. We make quality things,
                          we give quality advice and we only hire quality
                          people. People who want the best come to us for a
                          reason.
                        </p>
                      </div>
                    </div>

                    <div className="carousel__plane">
                      <div className="carousel__image">
                        <div
                          className="background-image"
                          style={{
                            backgroundImage: `url('assets/img/about/value2.jpg')`,
                          }}
                        ></div>
                      </div>
                      <div className="carousel__text">
                        <span className="carousel__num font_caps">/ 02</span>
                        <h4>
                          The experience
                          <br />
                          is everything.
                        </h4>
                        <p>
                          Our focus has always been to create enjoyable,
                          intuitive, engaging and remarkable experiences for
                          people. The experiences we craft are what we truly
                          value and it’s what sets us apart from everyone else.
                        </p>
                      </div>
                    </div>

                    <div className="carousel__plane">
                      <div className="carousel__image">
                        <div
                          className="background-image"
                          style={{
                            backgroundImage: `url('assets/img/about/value3.jpg')`,
                          }}
                        ></div>
                      </div>
                      <div className="carousel__text">
                        <span className="carousel__num font_caps">/ 03</span>
                        <h4>
                          Do things once
                          <br />
                          and do them right.
                        </h4>
                        <p>
                          We don’t do anything by halves. We only do things the
                          right way with care, know-how and effort. Corners are
                          an important part of any journey and we don’t cut
                          them, ever.
                        </p>
                      </div>
                    </div>

                    <div className="carousel__plane">
                      <div className="carousel__image">
                        <div
                          className="background-image"
                          style={{
                            backgroundImage: `url('assets/img/about/value4.jpg')`,
                          }}
                        ></div>
                      </div>
                      <div className="carousel__text">
                        <span className="carousel__num font_caps">/ 04</span>
                        <h4>
                          Form and function
                          <br />
                          need each other.
                        </h4>
                        <p>
                          What is the point in function without form or form
                          without function? We don’t believe in one without the
                          other. A balance of exquisite form and powerful
                          function is a formula for success.
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="carousel__progress">
                    <div className="carousel__progress-bar"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
}

export default About;