import React from "react";

const Careers = () => {
 return (
   <>
     <div className="header_animate active">
       <section id="q_slide" className="section section__header">
         <div className="q-container container-inner">
           <div className="columns">
             <div className="column menu_fade">hello Careers</div>
           </div>
         </div>
       </section>
     </div>
   </>
 );
}

export default Careers;
