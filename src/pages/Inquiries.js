import React from "react";

const Inquiries = () => {
  return (
    <>
      <div className="header_animate active">
        <section className="section section__header">
          <div className="q-container container-inner">
            <div className="columns">
              <div className="column menu_fade">hello Inquiries</div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}

export default Inquiries;
