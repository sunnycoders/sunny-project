import React from "react";
import Banner from "../components/Banner";
import ExHeading from "../components/experties/Heading";
import Experties from "../components/experties/Experties";
import Agency from "../components/Agency";
const Home = () => {
  
  return (
    <>
      <div id="barba-wrapper">
        <div className="barba-container" >
          <div data-scroll-container>
            <div id="main">
              <Banner />
              <section className="section small-pb x_hide" data-scroll-section>
                <ExHeading />
                <Experties />
              </section>
              <Agency />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
