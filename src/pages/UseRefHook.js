import React, { useRef } from "react";
import Props from "../components/Props";
const UseRefForm = () => {
  const ref = useRef();
  const ref1 = useRef();
  const ref2 = useRef();
  const ref3 = useRef();
  const submitHandler = (e) => {
    e.preventDefault();
    console.log(ref.current.value);
  };
  return (
    <>
      <Props text={{ name: 'sunny-73027275732'}} name={{data:'skype-id:sunny55544'}}/>
      <form>
        <input name="name" ref={ref} />
        <input name="email" ref={ref1} />
        <input name="tel" ref={ref2} />
        <input name="message" ref={ref3} />
        <button onClick={submitHandler}>submit</button>
      </form>
    </>
  );
};

export default UseRefForm;
